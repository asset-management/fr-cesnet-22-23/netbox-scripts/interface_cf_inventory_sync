from extras.scripts import *
from dcim.models import InventoryItemRole, Interface, InventoryItem
from extras.models import CustomField

__version__ = "0.1"
__author__ = "Michal Drobný"

# Timeout for the report run
JOB_TIMEOUT = 300

class InterfaceInventorySync(Script):

    job_timeout = JOB_TIMEOUT

    class Meta:
        name = "Synchronization Interface CF -> Inventory Item"
        description = "Synchronize inventory item with interface CF."
        field_order = ["inventory_role", "custom_field"]

    inventory_role = ObjectVar(
        model=InventoryItemRole,
        description="Inventory role",
        label="Inventory Role",
    )

    custom_field = ObjectVar(
        model=CustomField,
        description="Custom Field",
        label="Custom Field",
        query_params={
            "type": "object",
            "content_types": "dcim.interface"
        }
    )

    def run(self, data, commit):
        inventory_role = data.get("inventory_role")
        custom_field = data.get("custom_field")
        
        interfaces_with_inventory = Interface.objects.all()

        for interface in interfaces_with_inventory:
            if_inv_with_role = interface.inventory_items.filter(role=inventory_role).first()
            if if_inv_with_role is None:
                if interface.custom_field_data[custom_field.name]:
                    interface.custom_field_data[custom_field.name] = None
                    interface.save()
                    self.log_info(f"{interface.device} {interface.name} -> None")
                continue
            if interface.custom_field_data[custom_field.name] != if_inv_with_role.id:
                interface.custom_field_data[custom_field.name] = if_inv_with_role.id
                interface.save()
                self.log_success(f"{interface.device} {interface.name} -> {if_inv_with_role.name}")
            
        return "SUCCESS"
