# Interface CF Inventory Sync

## Popis
Tento skript je určen pro synchronizaci inventárních položek (Inventory Items) s volitelným polem (Custom Field) daného portu (Interface).

## Instalace
- **[Verze 3.4.X a níže]** Skript je nutno vložit do složky scripts, která má nejčastěji tuto cestu netbox/scripts/.
- **[Verze 3.5.X a výše]** Skript je nutné naimportovat z lokální složky. Cesta v Netboxu je Customization -> Scripts -> Add
- Viz https://docs.netbox.dev/en/stable/customization/custom-scripts/

## Výstup
![](/images/interface_cf_inventory_sync.gif)
![](/images/interface_cf_inventory_sync.png)

## Prerekvizity
1. Vytvořen objekt Custom Field s Type: Object, Object type: DCIM->Inventory item, Content type: DCIM->Interface
2. Vytvořen objekt Inventory item role
3. Vytvořen objekt Inventory item s touto rolí

## Postup fungování skriptu
1. Skript zkontroluje vstupní argumenty, které zadal uživatel. Kontrolovat se bude to, jestli existuje Inventory Role a zdali jeden z Assidned models zadaného Custom Field je dcim.interface. Zkontrolováno bude také to, jestli jeho typ je object. Pokud kontrola neprojde, tak skript o tom uživatele informuje pomocí návratové hodnoty a logu.
2. Skript si vyfiltruje všechny Interfaces, které mají přidělené nějaký Inventory Item. 
3. Poté prochází po jednom všechny Interfaces a kontroluje, zdali jejich Inventory Item má Inventory Role rovnu té, co zadal do argumentů skriptu.
4. Pokud nalezne shodu, tak přiřadí object tohoto Inventory Item do Custom Field daného Interface.
5. V případě, že se u Interfacu nachází více Inventory Items s požadovanou Inventory Role, skript vybere první Inventory Item, na který narazí.

## Proměnné
**Pro správné fungování skriptu je nutné upravit globální proměnné.**

#### JOB_TIMEOUT
- Táto proměnna představuje timeout pro běh skriptu.
- Výchozí hodnota je nastavena na 300 vteřin.
```python
JOB_TIMEOUT = 300
```

## Argumenty

#### Inventory Role
 - Povinný
 - Tento argument slouží k zadání role, která se má hledat v inventárních položkách daného portu.
 - Pokud existuje více položek s touto hodnotou, tak se přidá pouze první nalezená.

#### Custom Field
 - Povinný
 - Tento argument volitelné pole u portu, do kterého se inventární položka vloží (jako link).
